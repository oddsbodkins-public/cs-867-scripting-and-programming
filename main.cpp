/*
    20-Sept-2022

    Parsing student table with C++
*/

#include <iostream>
#include <string>
#include <cstddef>

#include "roster.h"

using namespace std;

void assignmentInformation () {
    // Print out to the screen, via your application, the title, the programming language used, and your name.

    cout << "Assignment information\n";
    cout << "----------------------\n";
    cout << "Scripting and Programming - Applications - C867\n";
    cout << "Programming Language: C++\n";
    cout << "Name: John Smith\n\n";
}

int main () {

    const string studentData[] =
        {
            "A1,John,Smith,John1989@gm ail.com,20,30,35,40,SECURITY",
            "A2,Suzan,Erickson,Erickson_1990@gmailcom,19,50,30,40,NETWORK",
            "A3,Jack,Napoli,The_lawyer99yahoo.com,19,20,40,33,SOFTWARE",
            "A4,Erin,Black,Erin.black@comcast.net,22,50,58,40,SECURITY",
            "A5,John,Smith,jsmith@yale.edu,25,20,30,35,SOFTWARE"
        };

    assignmentInformation ();

    Roster classRoster;
    // Parse the student table data and add each student to classRoster.
    classRoster.parseDataTable (studentData, 5);

    // Convert the following pseudo code
    classRoster.printAll();
    cout << "\n";

    classRoster.printInvalidEmails();
    cout << "\n";

    // loop through classRosterArray and for each element, print days in course (current_object's student id)
    vector<Student> classRosterArray = classRoster.getClassRosterArray();

    cout << "Students average days in course\n";
    cout << "-------------------------------\n";
    for (vector<Student>::iterator it = classRosterArray.begin(); it != classRosterArray.end(); ++it) {
        classRoster.printAverageDaysInCourse (it->getStudentID ());
    }
    cout << "\n";


    classRoster.printByDegreeProgram (SOFTWARE);
    cout << "\n";

    // remove a student
    classRoster.remove("A3");
    classRoster.printAll();
    cout << "\n";

    // call a second time, should print an error
    classRoster.remove("A3");

    return 0;
}
