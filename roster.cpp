#include "roster.h"

Roster::Roster() {
}

Roster::~Roster() {
    // release memory from classRosterArray

    // loop and remove objects from std::vector
    for (unsigned int i = 0; i < this->_classRosterArray.size(); ++i) {
        this->_classRosterArray.erase (this->_classRosterArray.begin () + i);
    }

    // Force existing std::vector to clear memory by swapping with a new empty std::vector
    // See: https://stackoverflow.com/questions/10464992/c-delete-vector-objects-free-memory
    vector <Student> tempVector;
    this->_classRosterArray.swap (tempVector);
}

void Roster::parseDataTable(const string table[], int numberOfRows) {
    Student s;

    for (int i = 0; i < numberOfRows; i++) {
        s = this->_parseDataRow(table[i]);
        this->_classRosterArray.push_back(s);
    }
}

Student Roster::_parseDataRow(string row) {
    string columns[9];

    /*
        Search from the end of the string to the beginning for each comma.
        Store the characters between each comma in a std::string array.
    */

    size_t found = row.find_last_of(",");
    size_t searchStringPosition = row.size();
    int columnI = 8;
    int numberOfCommas = 0;

    // npos is used for verifying that a string is found with 'find()'
    // https://cplusplus.com/reference/string/string/npos/

    // Error checking. Loop once to verify that the string has 8 commas.
    while (found != string::npos) {
        numberOfCommas += 1;
        found = row.find_last_of(",", found - 1);
    }

    if (numberOfCommas != 8) {
        cout << "Error! Incorrect number of commas!\n";
        Student s("", "", "", "", 0, 0, 0, 0, SOFTWARE);
        return s;
    }

    // reset found position
    found = row.find_last_of(",");

    while (found != string::npos) {

        // (found + 1) - skip the comma character
        // get the characters between the new comma position and the previous comma position Ex: ",40,"
        columns[columnI] = row.substr(found + 1, searchStringPosition - (found + 1));

        searchStringPosition = found;
        columnI -= 1;

        // continue looping until the beginning of the string
        found = row.find_last_of(",", found - 1);

        // the final segment is from the beginning of the string (Position 0) to the position of the last found comma
        if (found == string::npos) {
            columns[columnI] = row.substr(0, searchStringPosition);
        }
    }

    // Convert types to what is expected by Student class
    int age = std::stoi(columns[4]);
    int daysInCourse1 = std::stoi(columns[5]);
    int daysInCourse2 = std::stoi(columns[6]);
    int daysInCourse3 = std::stoi(columns[7]);

    DegreeProgram dp = this->_parseEnumString(columns[8]);

    Student s(columns[0], columns[1], columns[2], columns[3], age, daysInCourse1, daysInCourse2, daysInCourse3, dp);
    return s;
}

DegreeProgram Roster::_parseEnumString(string s) {
    string security = "SECURITY";
    string software = "SOFTWARE";
    string network = "NETWORK";

    DegreeProgram dp;

    if (s.compare(security) == 0) {
        dp = DegreeProgram(SECURITY);
    }
    else if (s.compare(software) == 0) {
        dp = DegreeProgram(SOFTWARE);
    }
    else if (s.compare(network) == 0) {
        dp = DegreeProgram(NETWORK);
    }

    return dp;
}

void Roster::add(string studentID, string firstName, string lastName, string emailAddress, int age, int daysInCourse1, int daysInCourse2, int daysInCourse3, DegreeProgram degreeprogram) {
    Student s(studentID, firstName, lastName, emailAddress, age, daysInCourse1, daysInCourse2, daysInCourse3, degreeprogram);
    this->_classRosterArray.push_back(s);
}

void Roster::remove(string studentID) {
    bool found = false;
    string s;

    // loop through vector list
    for (unsigned int i = 0; i < this->_classRosterArray.size (); ++i) {
        s = this->_classRosterArray.at (i).getStudentID ();

        // student id matches
        if (studentID.compare(s) == 0) {
            found = true;
            this->_classRosterArray.erase (this->_classRosterArray.begin () + i);
        }
    }


    if (found == false) {
        cout << "Student ID '" << studentID << "' not found!\n";
    }
}

void Roster::printAll() {
    cout << "Student list\n";
    cout << "------------\n";

    // loop through vector list
    for (unsigned int i = 0; i < this->_classRosterArray.size (); ++i) {
        this->_classRosterArray.at (i).printStudent ();
    }
}

void Roster::printAverageDaysInCourse(string studentID) {
    int daysToCompleteCourse[3] = { 0, 0, 0 };
    int average = 0;
    string s;

    // loop through vector list
    for (unsigned int i = 0; i < this->_classRosterArray.size(); ++i) {
        s = this->_classRosterArray.at(i).getStudentID();

        // student id matches
        if (studentID.compare (s) == 0) {
            // get the days taken to complete a course
            daysToCompleteCourse[0] = this->_classRosterArray.at(i).getDaysToCompleteCourse(0);
            daysToCompleteCourse[1] = this->_classRosterArray.at(i).getDaysToCompleteCourse(1);
            daysToCompleteCourse[2] = this->_classRosterArray.at(i).getDaysToCompleteCourse(2);
        }
    }

    for (int i = 0; i < 3; i++) {
        average += daysToCompleteCourse[i];
    }

    average = average / 3;

    cout << "Student ID: '" << studentID << "':" << " average days in course: " << average << "\n";
}

void Roster::printInvalidEmails() {
    // e.  public void printInvalidEmails() that verifies student email addresses and displays all invalid email addresses to the user.

    cout << "Students with invalid e-mail\n";
    cout << "----------------------------\n";

    // loop through vector list
    for (unsigned int i = 0; i < this->_classRosterArray.size(); ++i) {

        if (this->_classRosterArray.at(i).isEmailValid() == false) {
            cout << "Student ID: '" << this->_classRosterArray.at(i).getStudentID() << "' has invalid e-mail address: '" << this->_classRosterArray.at(i).getEmailAddress() << "'\n";
        }
    }

}

void Roster::printByDegreeProgram(DegreeProgram dp) {
    cout << "Student list with specific degree program\n";
    cout << "-----------------------------------------\n";

    // loop through vector list
    for (unsigned int i = 0; i < this->_classRosterArray.size(); ++i) {
        if (this->_classRosterArray.at(i).matchesDegreeProgram(dp) == true) {
            this->_classRosterArray.at(i).printStudent();
        }
    }

}

vector<Student> Roster::getClassRosterArray() {
    return this->_classRosterArray;
}
