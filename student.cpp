#include <iostream>
#include "student.h"
#include "degree.h"

using namespace std;

/*

2.  Create each of the following functions in the Student class:

a.  an accessor (i.e., getter) for each instance variable from part D1

b.  a mutator (i.e., setter) for each instance variable from part D1

c.  All external access and changes to any instance variables of the Student class must be done using accessor and mutator functions.

d.  constructor using all of the input parameters provided in the table

e.  print() to print specific student data

*/


// Constructors
Student::Student() {
    this->_emailAddress = "";
    this->_studentID = "";
    this->_firstName = "";
    this->_lastName = "";
    this->_emailAddress = "";
    this->_age = 0;
    this->_degreeProgram = 0;

    // initialize the array
    for (int i = 0; i < 3; i++) {
        this->_daysToCompleteCourse [i] = 0;
    }
}

Student::Student (string studentID, string firstName, string lastName, string emailAddress, int age, int daysInCourse1, int daysInCourse2, int daysInCourse3, DegreeProgram degree) {
    this->_emailAddress = emailAddress;
    this->_studentID = studentID;
    this->_firstName = firstName;
    this->_lastName = lastName;
    this->_emailAddress = emailAddress;
    this->_age = age;
    this->_degreeProgram = degree;
    this->_daysToCompleteCourse [0] = daysInCourse1;
    this->_daysToCompleteCourse [1] = daysInCourse2;
    this->_daysToCompleteCourse [2] = daysInCourse3;
}

// Getter functions
string Student::getStudentID () {
    return this->_studentID;
}

string Student::getFirstName () {
    return this->_firstName;
}

string Student::getLastName () {
    return this->_lastName;
}

string Student::getEmailAddress () {
    return this->_emailAddress;
}

int Student::getAge () {
    return this->_age;
}

int Student::getDaysToCompleteCourse (int index) {
    // verify that the index is in range
    if (index > -1 && index < 3) {
        return this->_daysToCompleteCourse [index];
    }

    return 0;
}

int Student::getDegreeProgram () {
    return this->_degreeProgram;
}

// Setter functions
void Student::setStudentID (string studentID) {
    this->_studentID = studentID;
}

void Student::setFirstName (string firstName) {
    this->_firstName = firstName;
}

void Student::setLastName (string lastName) {
    this->_lastName = lastName;
}

void Student::setEmailAddress (string emailAddress) {
    this->_emailAddress = emailAddress;
}

void Student::setAge (int age) {
    this->_age = age;
}

void Student::setDegreeProgram (int degreeProgram) {
    this->_degreeProgram = degreeProgram;
}

void Student::setDaysToCompleteCourse (int daysToCompleteCourse1, int daysToCompleteCourse2, int daysToCompleteCourse3) {
    this->_daysToCompleteCourse [0] = daysToCompleteCourse1;
    this->_daysToCompleteCourse [1] = daysToCompleteCourse2;
    this->_daysToCompleteCourse [2] = daysToCompleteCourse3;
}

// Other functions

void Student::printStudentID () {
    cout << "Student ID: " << this->_studentID;
}

void Student::printFirstName () {
    cout << "First name: " << this->_firstName;
}

void Student::printLastName () {
    cout << "Last name: " << this->_lastName;
}

void Student::printEmailAddress () {
    cout << "Email address: " << this->_emailAddress;
}

void Student::printAge () {
    cout << "Age: " << this->_age;
}

void Student::printDaysToCompleteThreeCourses () {
    cout << "daysInCourse: {";
    cout << this->_daysToCompleteCourse [0] << ", ";
    cout << this->_daysToCompleteCourse [1] << ", ";
    cout << this->_daysToCompleteCourse [2] << "}";
}

void Student::printDegreeProgram () {
    switch (this->_degreeProgram) {
        case SECURITY:
            cout << "Degree program: Computer Security";
            break;

        case NETWORK:
            cout << "Degree program: Computer Networking";
            break;

        case SOFTWARE:
            cout << "Degree program: Computer Software";
            break;

        default:
            cout << "Degree program: Unknown";
            break;
    }

}

void Student::printStudent () {
    /*
    c. public void printAll() that prints a complete tab-separated list of student data in the provided format:

    "A1 [tab] First Name: John [tab] Last Name: Smith [tab] Age: 20 [tab]daysInCourse: {35, 40, 55} Degree Program: Security".

    The printAll() function should loop through all the students in classRosterArray and call the print() function for each student.

    */

    this->printStudentID ();
    cout << "\t";

    this->printFirstName ();
    cout << "\t";

    this->printLastName ();
    cout << "\t";

    this->printAge ();
    cout << "\t";

    this->printDaysToCompleteThreeCourses();
    cout << "\t";

    this->printDegreeProgram ();
    cout << endl;
}

bool Student::isEmailValid () {
    // Note: A valid email should include an at sign ('@') and period ('.') and should not include a space (' ').

    // verify that the e-mail address contains an '@' character
    size_t f = this->_emailAddress.find ("@");

    if (f == string::npos) {
        //cout << "Invalid e-mail address! No '@' (at) character found!\n";
        return false;
    }

    // verify that the e-mail address contains an '.' character
    f = this->_emailAddress.find (".");

    if (f == string::npos) {
        //cout << "Invalid e-mail address! No '.' (period) character found!\n";
        return false;
    }

    // verify that the e-mail address does not contain a ' ' (space) character

    f = this->_emailAddress.find (" ");

    if (f != string::npos) {
        //cout << "Invalid e-mail address! A ' ' (space) character was found!\n";
        return false;
    }

    return true;
}

bool Student::matchesDegreeProgram (DegreeProgram dp) {
    if (this->_degreeProgram == dp) {
        return true;
    }

    return false;
}
