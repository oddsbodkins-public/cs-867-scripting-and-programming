Cross platform C++ console application.

Uses the Codeblocks IDE (https://www.codeblocks.org/downloads/).

Development Environment
-----------------------
* Debian Linux 12 x86_64
* Kernel 5.18.16-1 (2022-08-10)
* g++ Version: (Debian 12.1.0-8) 12.1.0

Also tested with Visual Studio Code on Windows 10. 
