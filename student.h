/*

1.  Create the class Student  in the files student.h and student.cpp, which includes each of the following variables:

•  student ID

•  first name

•   last name

•  email address

•  age

•  array of number of days to complete each course

•  degree program
*/

#include <string>
#include "degree.h"

using namespace std;

class Student {

    // Variables

    private:
        string _studentID = "";
        string _firstName = "";
        string _lastName = "";
        string _emailAddress = "";
        int _age = -1;
        int _degreeProgram = -1;
        int _daysToCompleteCourse [3];

    public:
        // Constructors
        Student ();
        Student (string, string, string, string, int, int, int, int, DegreeProgram);

        // Getter functions
        string getStudentID ();
        string getFirstName ();
        string getLastName ();
        string getEmailAddress ();
        int getAge ();
        int getDaysToCompleteCourse (int);
        int getDegreeProgram ();

        // Setter functions
        void setStudentID (string);
        void setFirstName (string);
        void setLastName (string);
        void setEmailAddress (string);
        void setAge (int);
        void setDegreeProgram (int);
        void setDaysToCompleteCourse (int, int, int);

        // Other functions
        void printStudent ();
        void printStudentID ();
        void printDaysToCompleteThreeCourses ();
        void printEmailAddress ();
        void printFirstName ();
        void printLastName ();
        void printAge ();
        void printDegreeProgram ();
        void printAverageDaysInCourse ();
        bool isEmailValid ();
        bool matchesDegreeProgram (DegreeProgram);
};
